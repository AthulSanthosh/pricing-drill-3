const toggle = document.getElementById('toggle-btn');
const boxContainer = document.getElementById('boxes');

toggle.addEventListener('change',(e)=>{
    boxContainer.classList.toggle('toggled')
});
